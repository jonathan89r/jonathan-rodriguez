/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import com.mysql.jdbc.Connection;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import javax.faces.context.FacesContext;

/**
 *
 * @author jonathan.rodriguez
 */
@ManagedBean
@ViewScoped
public class tiquete {

    public void mostrarTiquete() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, JRException, IOException {
        Connection conexion;
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conexion = (Connection) DriverManager.getConnection("jdbc:mysql://usam-sql.sv.cds:3306/airfic", "kz", "kzroot");
        File reportFile = new File("C:\\Users\\jonathan.rodriguez\\Documents\\SI_COMPRAS\\proyecto_venta_boletos\\airfic\\web\\jasper\\tiquete.jasper");
        Map parameters = new HashMap();
        parameters.put("nombre_parametro", "valor_parametro");
        JasperPrint jp = JasperFillManager.fillReport(reportFile.getPath(), parameters, conexion);

        byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, conexion);
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.addHeader("Content-disposition", "attachment; filename=tiquete.pdf");
        ServletOutputStream os = response.getOutputStream();

        JasperExportManager.exportReportToPdfStream(jp, os);
        FacesContext.getCurrentInstance().responseComplete();
    }
}
