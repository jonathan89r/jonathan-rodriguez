/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import mantenimiento.validacion_login;
import persistencia.LoginUsuario;

/**
 *
 * @author jonathan.rodriguez
 */
@ManagedBean
@RequestScoped
@SessionScoped
public class bean_validacion {

    private LoginUsuario loggin;
    private LoginUsuario get;
    private validacion_login val = new validacion_login();

    @PostConstruct
    public void init() {
        this.loggin = new LoginUsuario();
        get = (LoginUsuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("a");
    }

    /**
     * @return the loggin
     */
    public LoginUsuario getLoggin() {
        return loggin;
    }

    /**
     * @param loggin the loggin to set
     */
    public void setLoggin(LoginUsuario loggin) {
        this.loggin = loggin;
    }

    public void validar() throws IOException {
        LoginUsuario log = val.consultarLogin(loggin.getUsuario(), loggin.getPass());
        String mensaje = "";

        if (log != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("a", log);
            if (log.getPass() != null && log.getTipoUsuario().getIdTipo() == 4) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("faces/index.xhtml?faces-redirect-true");
            } else {
                mensaje = "contraseña es incorrecta";
            }
        } else {
            mensaje = "Datos erroneos, intentelo de nuevo.";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void cerrarSession() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }

    /**
     * @return the get
     */
    public LoginUsuario getGet() {
        return get;
    }

    /**
     * @param get the get to set
     */
    public void setGet(LoginUsuario get) {
        this.get = get;
    }

}
