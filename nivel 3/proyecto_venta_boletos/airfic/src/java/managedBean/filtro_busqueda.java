/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import mantenimiento.mantenimiento_filtro;
import persistencia.AvionDestino;
import persistencia.Destino;
import persistencia.Ofertasvuelos;
import persistencia.Origen;
import persistencia.Vuelosv;

/**
 *
 * @author jonathan.rodriguez
 */
@ManagedBean
@ViewScoped
public class filtro_busqueda implements Serializable {

    private List<Origen> origen;
    private List<Destino> destino;
    private List<Ofertasvuelos> listaOfertas;
    private List<Vuelosv> listaVuelos;
    mantenimiento_filtro m = new mantenimiento_filtro();
    private List<Vuelosv> s;
    private List<AvionDestino> avionDestino;
    private AvionDestino j;
    private Ofertasvuelos ofertas;
    private Vuelosv vuelos;

    private String origenP;
    private String destinoP;
    private Double precio;
    private String clase;
    private String vuelo;
    private String aerolinea;

    private int idG;

    @PostConstruct
    public void init() {
        this.vuelos = new Vuelosv();
        this.j = new AvionDestino();
        this.ofertas = new Ofertasvuelos();
    }

    public void mostrarPodID() {
        String mensaje = "";
        Ofertasvuelos offet = m.buscarOfertas(idG);

        if (offet != null) {
            origenP = offet.getOrigen();
            destinoP = offet.getDestino();
            precio = offet.getPrecio();
            clase = offet.getClase();
            vuelo = offet.getVuelo();
            aerolinea = offet.getNombreAerolinea();
        } else {
            mensaje = "Datos no encontrados";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

//    public void buscar() throws IOException {
//
//        Vuelosv v = m.buscar(this.idG);
//
//        String mensaje = "";
//        if (v != null) {
//            mensaje = "Datos encontrados";
//        } else {
//            mensaje = "Error";
//        }
//        FacesMessage msg = new FacesMessage(mensaje);
//        FacesContext.getCurrentInstance().addMessage(null, msg);
//
//    }

    public void buscar2(int id, int id2) throws IOException {

        AvionDestino b = m.consultarVuelos(id2);
        AvionDestino a = m.consultarVuelos2(id);

        String mensaje = "";
        if (a != null && b != null) {
            mensaje = "Datos encontrados";
            FacesContext.getCurrentInstance().getExternalContext().redirect("faces/vuelos.xhtml");

        } else {
            mensaje = "Error";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);

    }

    public List<Origen> getOrigen() {
        origen = m.consultarOrigen();
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(List<Origen> origen) {
        this.origen = origen;
    }

    /**
     * @return the destino
     */
    public List<Destino> getDestino() {
        destino = m.consultarDestino();
        return destino;
    }

    public List<Ofertasvuelos> getListaOfertas() {
        listaOfertas = m.consultarOfertas();
        return listaOfertas;
    }

    /**
     * @return the j
     */
    public AvionDestino getJ() {
        return j;
    }

    /**
     * @param j the j to set
     */
    public void setJ(AvionDestino j) {
        this.j = j;
    }

    /**
     * @return the vuelos
     */
    public Vuelosv getVuelos() {
        return vuelos;
    }

    /**
     * @param vuelos the vuelos to set
     */
    public void setVuelos(Vuelosv vuelos) {
        this.vuelos = vuelos;
    }

    /**
     * @return the listaVuelos
     */
    public List<Vuelosv> getListaVuelos() {
        listaVuelos = m.consultarVuelos();
        return listaVuelos;
    }

    /**
     * @return the avionDestino
     */
    public List<AvionDestino> getAvionDestino() {
        avionDestino = m.consultarAvionDestino();
        return avionDestino;
    }

    /**
     * @return the origenP
     */
    public String getOrigenP() {
        return origenP;
    }

    /**
     * @param origenP the origenP to set
     */
    public void setOrigenP(String origenP) {
        this.origenP = origenP;
    }

    /**
     * @return the destinoP
     */
    public String getDestinoP() {
        return destinoP;
    }

    /**
     * @param destinoP the destinoP to set
     */
    public void setDestinoP(String destinoP) {
        this.destinoP = destinoP;
    }

    /**
     * @return the precio
     */
    public Double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    /**
     * @return the clase
     */
    public String getClase() {
        return clase;
    }

    /**
     * @param clase the clase to set
     */
    public void setClase(String clase) {
        this.clase = clase;
    }

    /**
     * @return the vuelo
     */
    public String getVuelo() {
        return vuelo;
    }

    /**
     * @param vuelo the vuelo to set
     */
    public void setVuelo(String vuelo) {
        this.vuelo = vuelo;
    }

    /**
     * @return the aerolinea
     */
    public String getAerolinea() {
        return aerolinea;
    }

    /**
     * @param aerolinea the aerolinea to set
     */
    public void setAerolinea(String aerolinea) {
        this.aerolinea = aerolinea;
    }

    /**
     * @return the idG
     */
    public int getIdG() {
        return idG;
    }

    /**
     * @param idG the idG to set
     */
    public void setIdG(int idG) {
        this.idG = idG;
    }

}
