/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import mantenimiento.mantenimiento_filtro;
import mantenimiento.mantenimiento_tiquete;
import persistencia.AvionDestino;
import persistencia.LoginUsuario;
import persistencia.Ofertasvuelos;
import persistencia.TipoDocumento;
import persistencia.Tiquete;
import persistencia.Vuelosv;

/**
 *
 * @author jonathan.rodriguez
 */
@ManagedBean
@ViewScoped
public class bean_tiquete {

    private Tiquete tiquete;

    private int idG;
    private int idG2;
    private int idG3;

    private int id_usuario;
    private String nombre_uno;
    private String nombre_dos;
    private String apellido_uno;
    private String apellido_dos;
    private String telefono;
    private String direccion;
    private int id_documento;
    private String documento;
    private int id_pasajero;

    private Double valor;
    private String fecha_vuelo;
    private int numero_puesto;
    private Double iva;
    private Double total;
    private String hora_salida;
    private int destino;
    private int codigo = 323434;
    private int tipo_pago;
    private String photo;

    private String numeroTarjeta;
    private int id_destino;

    private String origen;
    private String destinop;
    private Double precio;
    private String clase;
    private String vuelo;
    private String aerolinea;

    private int codigoBarra;

    private Ofertasvuelos ofertas;
    private List<Ofertasvuelos> listaOfertas;
    mantenimiento_tiquete m = new mantenimiento_tiquete();
    mantenimiento_filtro ma = new mantenimiento_filtro();
    private List<TipoDocumento> tipo_documento;
    private Ofertasvuelos offet;
    private LoginUsuario get;
    private Ofertasvuelos aa;
    private List<Tiquete> ti;

    private String codigo2 = Integer.toString(codigo);

    public String getCodigo2() {
        return codigo2;
    }

    public void setCodigo2(String codigo2) {
        this.codigo2 = codigo2;
    }

    private Tiquete tiquete2;
    private List<Vuelosv> destinos;
    private Vuelosv vuelosv;

    private Vuelosv vuelosvv;

    @PostConstruct
    public void init() {
        this.offet = new Ofertasvuelos();
        this.setTiquete2(new Tiquete());
        this.setTiquete(new Tiquete());
        this.setOfertas(new Ofertasvuelos());
        get = (LoginUsuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("a");
        aa = (Ofertasvuelos) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("x");
        vuelosvv = (Vuelosv) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("xs");
        this.vuelosv = new Vuelosv();
    }

//    ----------------------------------------------------------------------------------------------------------
    //metodo para guardar con vista (ofertasVuelos)
    public void guardar() throws IOException {
        String mensaje = "";

        LoginUsuario gets = (LoginUsuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("a");
        if (gets != null) {

            int lenght = this.nombre_uno.length() + this.apellido_dos.length() + get.getUsuario().length() + get.getIdLogin();
            int lenght2 = this.nombre_uno.length() + this.apellido_dos.length() + get.getUsuario().length() + get.getIdLogin();
            int nR = (int) (Math.random() * 2213 + 57);
            String n1 = String.valueOf(this.nombre_uno.length() + this.apellido_dos.length());
            String n2 = String.valueOf(get.getUsuario().length() + get.getIdLogin());
            codigoBarra = Integer.parseInt(n1.concat(n2) + nR);

            if (m.guardarTiquete(get.getIdLogin(),
                    nombre_uno,
                    nombre_dos,
                    apellido_uno,
                    apellido_dos,
                    telefono,
                    direccion,
                    id_documento,
                    documento,
                    aa.getPrecio(),
                    fecha_vuelo,
                    aa.getPrecio() * 0.13,
                    (aa.getPrecio() * 0.13) + aa.getPrecio(),
                    "6:00 AM",
                    aa.getIdAvionDestino(),
                    codigoBarra,
                    1) != 1) {
                //se limpian los campos para evitar problemas
                this.nombre_uno = "";
                this.nombre_dos = "";
                this.apellido_uno = "";
                this.apellido_dos = "";
                this.telefono = "";
                this.documento = "";
                this.fecha_vuelo = "";
                this.direccion = "";
                this.numeroTarjeta = "";
                mensaje = "Datos guardados";

                FacesContext.getCurrentInstance().getExternalContext().redirect("faces/afTicket.xhtml");
            }
        } else {
            mensaje = "Necesita iniciar sesion para continuar";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    //metodo para guardar vuelos (vista vuelosSV)
    public void guardarVuelo() throws IOException {
        String mensaje = "";

        if (get != null) {

            //codigo random provicional
            int lenght = this.nombre_uno.length() + this.apellido_dos.length() + get.getUsuario().length() + get.getIdLogin();
            int lenght2 = this.nombre_uno.length() + this.apellido_dos.length() + get.getUsuario().length() + get.getIdLogin();
            int nR = (int) (Math.random() * 2213 + 57);
            String n1 = String.valueOf(this.nombre_uno.length() + this.apellido_dos.length());
            String n2 = String.valueOf(get.getUsuario().length() + get.getIdLogin());
            codigoBarra = Integer.parseInt(n1.concat(n2) + nR);

            if (m.guardarTiquete(get.getIdLogin(),
                    nombre_uno,
                    nombre_dos,
                    apellido_uno,
                    apellido_dos,
                    telefono,
                    direccion,
                    id_documento,
                    documento,
                    vuelosvv.getPrecio(),
                    fecha_vuelo,
                    vuelosvv.getPrecio() * 0.13,
                    (vuelosvv.getPrecio() * 0.13) + vuelosvv.getPrecio(),
                    "6:00 AM",
                    vuelosvv.getIdAvionDestino(),
                    codigoBarra,
                    1) != 1) {
                //se limpian los campos para evitar problemas
                this.nombre_uno = "";
                this.nombre_dos = "";
                this.apellido_uno = "";
                this.apellido_dos = "";
                this.telefono = "";
                this.documento = "";
                this.fecha_vuelo = "";
                this.direccion = "";
                this.numeroTarjeta = "";

                mensaje = "Datos guardados";
            } else {
                mensaje = "Error, vuelva a ingresar los datos";
            }
        } else {
            mensaje = "Necesita iniciar sesion para continuar";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

//  -----------------------------------------------------------------------------------------------------------------------  
//    --------------------------------------------------------------------------------------------------------------------
//    metodo para mostar por un id, las ofertas de vuelos
    public void mostrarPodIDVuelo() {
        offet = ma.buscarOfertas(this.idG);

        if (offet != null) {
            Ofertasvuelos a = (Ofertasvuelos) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("x", offet);
            origen = offet.getOrigen();
            destinop = offet.getDestino();
            this.destino = offet.getIdAvionDestino();
            this.precio = offet.getPrecio();
            clase = offet.getClase();
            this.photo = offet.getPhoto();
            setVuelo(offet.getVuelo());
            aerolinea = offet.getNombreAerolinea();
        }

    }

    //    metodo para mostar por un id, todos los vuelos
    public void mostrarPodIDTodosVuelos() {
        String mensaje = "";
        vuelosv = m.buscarVuelosSV(this.idG2);

        if (vuelosv != null) {
            Vuelosv a = (Vuelosv) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("xs", vuelosv);
            origen = vuelosv.getOrigen();
            destinop = vuelosv.getDestino();
            this.destino = vuelosv.getIdAvionDestino();
            this.photo = vuelosv.getPhoto();
            this.precio = vuelosv.getPrecio();
            clase = vuelosv.getClase();
            setVuelo(vuelosv.getVuelo());
            aerolinea = vuelosv.getNombreAerolinea();
        }

    }

    //filtro de busqueda 
    public void filtroBusqueda() {
        Tiquete t = m.consultarTiquete(this.id_destino);

        if (t != null) {
            origen = t.getDesdeHacia().getIdOrigenDestino().getIdOrigen().getNombre();
            destinop = t.getDesdeHacia().getIdOrigenDestino().getIdDestino().getNombre();
            this.destino = t.getDesdeHacia().getIdAvionDestino();
            this.precio = t.getDesdeHacia().getIdOrigenDestino().getPrecio();
            clase = t.getDesdeHacia().getIdOrigenDestino().getClase().getClase();
            vuelo = t.getDesdeHacia().getTipoVuelo().getVuelo();
            aerolinea = t.getDesdeHacia().getIdAvion().getAerolinea().getNombreAerolinea();
        }
    }

//    --------------------------------------------------------------------------------------------------------------------
    public void mostrarPorID() {
        String mensaje = "";

        Tiquete tiq = m.consultarTiquete(this.codigoBarra);

        if (tiq != null) {
            mensaje = "Datos encontrados";

            this.id_pasajero = tiq.getIdPasajero().getIdPasajero();
            this.precio = tiq.getValor();
            this.fecha_vuelo = tiq.getFechaVuelo();
            this.numero_puesto = tiq.getNumeroPuesto();
            this.iva = tiq.getIva();
            this.total = tiq.getTotal();
            this.hora_salida = tiq.getHoraSalida();
            this.destino = tiq.getCodigo();
            this.tipo_pago = tiq.getTipoPago().getIdTipoPago();

        } else {
            mensaje = "Datos no encontrados";
        }
    }

    /**
     * @return the tipo_documento
     */
    public List<TipoDocumento> getTipo_documento() {
        tipo_documento = m.consultarTDocumento();
        return tipo_documento;
    }

    /**
     * @return the id_usuario
     */
    public int getId_usuario() {
        return id_usuario;
    }

    /**
     * @param id_usuario the id_usuario to set
     */
    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    /**
     * @return the nombre_uno
     */
    public String getNombre_uno() {
        return nombre_uno;
    }

    /**
     * @param nombre_uno the nombre_uno to set
     */
    public void setNombre_uno(String nombre_uno) {
        this.nombre_uno = nombre_uno;
    }

    /**
     * @return the nombre_dos
     */
    public String getNombre_dos() {
        return nombre_dos;
    }

    /**
     * @param nombre_dos the nombre_dos to set
     */
    public void setNombre_dos(String nombre_dos) {
        this.nombre_dos = nombre_dos;
    }

    /**
     * @return the apellido_uno
     */
    public String getApellido_uno() {
        return apellido_uno;
    }

    /**
     * @param apellido_uno the apellido_uno to set
     */
    public void setApellido_uno(String apellido_uno) {
        this.apellido_uno = apellido_uno;
    }

    /**
     * @return the apellido_dos
     */
    public String getApellido_dos() {
        return apellido_dos;
    }

    /**
     * @param apellido_dos the apellido_dos to set
     */
    public void setApellido_dos(String apellido_dos) {
        this.apellido_dos = apellido_dos;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the id_documento
     */
    public int getId_documento() {
        return id_documento;
    }

    /**
     * @param id_documento the id_documento to set
     */
    public void setId_documento(int id_documento) {
        this.id_documento = id_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the valor
     */
    public Double getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(Double valor) {
        this.valor = valor;
    }

    /**
     * @return the fecha_vuelo
     */
    public String getFecha_vuelo() {
        return fecha_vuelo;
    }

    /**
     * @param fecha_vuelo the fecha_vuelo to set
     */
    public void setFecha_vuelo(String fecha_vuelo) {
        this.fecha_vuelo = fecha_vuelo;
    }

    /**
     * @return the numero_puesto
     */
    public int getNumero_puesto() {
        return numero_puesto;
    }

    /**
     * @param numero_puesto the numero_puesto to set
     */
    public void setNumero_puesto(int numero_puesto) {
        this.numero_puesto = numero_puesto;
    }

    /**
     * @return the iva
     */
    public Double getIva() {
        return iva;
    }

    /**
     * @param iva the iva to set
     */
    public void setIva(Double iva) {
        this.iva = iva;
    }

    /**
     * @return the total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * @return the hora_salida
     */
    public String getHora_salida() {
        return hora_salida;
    }

    /**
     * @param hora_salida the hora_salida to set
     */
    public void setHora_salida(String hora_salida) {
        this.hora_salida = hora_salida;
    }

    /**
     * @return the destino
     */
    public int getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(int destino) {
        this.destino = destino;
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the tipo_pago
     */
    public int getTipo_pago() {
        return tipo_pago;
    }

    /**
     * @param tipo_pago the tipo_pago to set
     */
    public void setTipo_pago(int tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    /**
     * @return the destinop
     */
    public String getDestinop() {
        return destinop;
    }

    /**
     * @param destinop the destinop to set
     */
    public void setDestinop(String destinop) {
        this.destinop = destinop;
    }

    /**
     * @return the precio
     */
    public Double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    /**
     * @return the clase
     */
    public String getClase() {
        return clase;
    }

    /**
     * @param clase the clase to set
     */
    public void setClase(String clase) {
        this.clase = clase;
    }

    /**
     * @return the vuelo
     */
    public String getVuelo() {
        return vuelo;
    }

    /**
     * @param vuelo the vuelo to set
     */
    public void setVuelo(String vuelo) {
        this.vuelo = vuelo;
    }

    /**
     * @return the aerolinea
     */
    public String getAerolinea() {
        return aerolinea;
    }

    /**
     * @param aerolinea the aerolinea to set
     */
    public void setAerolinea(String aerolinea) {
        this.aerolinea = aerolinea;
    }

    /**
     * @return the ofertas
     */
    public Ofertasvuelos getOfertas() {
        return ofertas;
    }

    /**
     * @param ofertas the ofertas to set
     */
    public void setOfertas(Ofertasvuelos ofertas) {
        this.ofertas = ofertas;
    }

    /**
     * @return the listaOfertas
     */
    public List<Ofertasvuelos> getListaOfertas() {
        mantenimiento_filtro ma = new mantenimiento_filtro();
        listaOfertas = ma.consultarOfertas();
        return listaOfertas;
    }

    /**
     * @param listaOfertas the listaOfertas to set
     */
    public void setListaOfertas(List<Ofertasvuelos> listaOfertas) {
        this.listaOfertas = listaOfertas;
    }

    /**
     * @return the offet
     */
    public Ofertasvuelos getOffet() {
        return offet;
    }

    /**
     * @param offet the offet to set
     */
    public void setOffet(Ofertasvuelos offet) {
        this.offet = offet;
    }

    /**
     * @return the get
     */
    public LoginUsuario getGet() {
        return get;
    }

    /**
     * @param get the get to set
     */
    public void setGet(LoginUsuario get) {
        this.get = get;
    }

    /**
     * @return the aa
     */
    public Ofertasvuelos getAa() {
        return aa;
    }

    /**
     * @param aa the aa to set
     */
    public void setAa(Ofertasvuelos aa) {
        this.aa = aa;
    }

    /**
     * @return the codigoBarra
     */
    public int getCodigoBarra() {
        return codigoBarra;
    }

    /**
     * @param codigoBarra the codigoBarra to set
     */
    public void setCodigoBarra(int codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    /**
     * @return the numeroTarjeta
     */
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    /**
     * @param numeroTarjeta the numeroTarjeta to set
     */
    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    /**
     * @return the ti
     */
    public List<Tiquete> getTi() {

        return ti;
    }

    /**
     * @param ti the ti to set
     */
    public void setTi(List<Tiquete> ti) {
        this.ti = ti;
    }

    /**
     * @return the id_pasajero
     */
    public int getId_pasajero() {
        return id_pasajero;
    }

    /**
     * @param id_pasajero the id_pasajero to set
     */
    public void setId_pasajero(int id_pasajero) {
        this.id_pasajero = id_pasajero;
    }

    /**
     * @return the idG
     */
    public int getIdG() {
        return idG;
    }

    /**
     * @param idG the idG to set
     */
    public void setIdG(int idG) {
        this.idG = idG;
    }

    /**
     * @return the vuelosv
     */
    public Vuelosv getVuelosv() {
        return vuelosv;
    }

    /**
     * @param vuelosv the vuelosv to set
     */
    public void setVuelosv(Vuelosv vuelosv) {
        this.vuelosv = vuelosv;
    }

    /**
     * @return the idG2
     */
    public int getIdG2() {
        return idG2;
    }

    /**
     * @param idG2 the idG2 to set
     */
    public void setIdG2(int idG2) {
        this.idG2 = idG2;
    }

    /**
     * @return the destinos
     */
    public List<Vuelosv> getDestinos() {
        destinos = m.consultarVuelossv();
        return destinos;
    }

    /**
     * @param destinos the destinos to set
     */
    public void setDestinos(List<Vuelosv> destinos) {
        this.destinos = destinos;
    }

    /**
     * @return the vuelosvv
     */
    public Vuelosv getVuelosvv() {
        return vuelosvv;
    }

    /**
     * @param vuelosvv the vuelosvv to set
     */
    public void setVuelosvv(Vuelosv vuelosvv) {
        this.vuelosvv = vuelosvv;
    }

    /**
     * @return the tiquete2
     */
    public Tiquete getTiquete2() {
        return tiquete2;
    }

    /**
     * @param tiquete2 the tiquete2 to set
     */
    public void setTiquete2(Tiquete tiquete2) {
        this.tiquete2 = tiquete2;
    }

    /**
     * @return the idG3
     */
    public int getIdG3() {
        return idG3;
    }

    /**
     * @param idG3 the idG3 to set
     */
    public void setIdG3(int idG3) {
        this.idG3 = idG3;
    }

    /**
     * @return the id_destino
     */
    public int getId_destino() {
        return id_destino;
    }

    /**
     * @param id_destino the id_destino to set
     */
    public void setId_destino(int id_destino) {
        this.id_destino = id_destino;

    }

    public void consultarTike(int id) {

        String msj = "";
        this.setTiquete(m.consultarTiquete(id));

        if (getTiquete() != null) {
            msj = "Si paso xdxdxdxd";
            this.destino = getTiquete().getDesdeHacia().getIdOrigenDestino().getIdDestino().getIdDestino();
            this.fecha_vuelo = getTiquete().getFechaVuelo();
            this.origen = getTiquete().getDesdeHacia().getIdOrigenDestino().getIdOrigen().getNombre();
            this.hora_salida = getTiquete().getHoraSalida();
            this.destinop = getTiquete().getDesdeHacia().getIdOrigenDestino().getIdDestino().getNombre();
            this.id_pasajero = getTiquete().getIdPasajero().getIdPasajero();
        } else {
            msj = "Datos no encontrados";
            FacesMessage m = new FacesMessage(msj);
            FacesContext.getCurrentInstance().addMessage(null, m);
        }
    }

    /**
     * @return the photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     * @return the tiquete
     */
    public Tiquete getTiquete() {
        return tiquete;
    }

    /**
     * @param tiquete the tiquete to set
     */
    public void setTiquete(Tiquete tiquete) {
        this.tiquete = tiquete;
    }

}
