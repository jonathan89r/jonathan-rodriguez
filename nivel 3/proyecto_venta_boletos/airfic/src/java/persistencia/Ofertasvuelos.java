/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jonathan.rodriguez
 */
@Entity
@Table(name = "ofertasvuelos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ofertasvuelos.findAll", query = "SELECT o FROM Ofertasvuelos o")
    , @NamedQuery(name = "Ofertasvuelos.findByIdAvionDestino", query = "SELECT o FROM Ofertasvuelos o WHERE o.idAvionDestino = :idAvionDestino")
    , @NamedQuery(name = "Ofertasvuelos.findByIdOrigenDestino", query = "SELECT o FROM Ofertasvuelos o WHERE o.idOrigenDestino = :idOrigenDestino")
    , @NamedQuery(name = "Ofertasvuelos.findByIdOrigen", query = "SELECT o FROM Ofertasvuelos o WHERE o.idOrigen = :idOrigen")
    , @NamedQuery(name = "Ofertasvuelos.findByIdDestino", query = "SELECT o FROM Ofertasvuelos o WHERE o.idDestino = :idDestino")
    , @NamedQuery(name = "Ofertasvuelos.findByPlaca", query = "SELECT o FROM Ofertasvuelos o WHERE o.placa = :placa")
    , @NamedQuery(name = "Ofertasvuelos.findByNombreAerolinea", query = "SELECT o FROM Ofertasvuelos o WHERE o.nombreAerolinea = :nombreAerolinea")
    , @NamedQuery(name = "Ofertasvuelos.findByVuelo", query = "SELECT o FROM Ofertasvuelos o WHERE o.vuelo = :vuelo")
    , @NamedQuery(name = "Ofertasvuelos.findByClase", query = "SELECT o FROM Ofertasvuelos o WHERE o.clase = :clase")
    , @NamedQuery(name = "Ofertasvuelos.findByOrigen", query = "SELECT o FROM Ofertasvuelos o WHERE o.origen = :origen")
    , @NamedQuery(name = "Ofertasvuelos.findByDestino", query = "SELECT o FROM Ofertasvuelos o WHERE o.destino = :destino")
    , @NamedQuery(name = "Ofertasvuelos.findByPrecio", query = "SELECT o FROM Ofertasvuelos o WHERE o.precio = :precio")
    , @NamedQuery(name = "Ofertasvuelos.findByPhoto", query = "SELECT o FROM Ofertasvuelos o WHERE o.photo = :photo")})
public class Ofertasvuelos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "id_avion_destino")
    @Id
    private int idAvionDestino;
    @Basic(optional = false)
    @Column(name = "id_origen_destino")
    private int idOrigenDestino;
    @Basic(optional = false)
    @Column(name = "id_origen")
    private int idOrigen;
    @Basic(optional = false)
    @Column(name = "id_destino")
    private int idDestino;
    @Basic(optional = false)
    @Column(name = "placa")
    private String placa;
    @Basic(optional = false)
    @Column(name = "nombre_aerolinea")
    private String nombreAerolinea;
    @Basic(optional = false)
    @Column(name = "vuelo")
    private String vuelo;
    @Basic(optional = false)
    @Column(name = "clase")
    private String clase;
    @Column(name = "origen")
    private String origen;
    @Basic(optional = false)
    @Column(name = "destino")
    private String destino;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio")
    private Double precio;
    @Column(name = "photo")
    private String photo;

    public Ofertasvuelos() {
    }

    public int getIdAvionDestino() {
        return idAvionDestino;
    }

    public void setIdAvionDestino(int idAvionDestino) {
        this.idAvionDestino = idAvionDestino;
    }

    public int getIdOrigenDestino() {
        return idOrigenDestino;
    }

    public void setIdOrigenDestino(int idOrigenDestino) {
        this.idOrigenDestino = idOrigenDestino;
    }

    public int getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(int idOrigen) {
        this.idOrigen = idOrigen;
    }

    public int getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(int idDestino) {
        this.idDestino = idDestino;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getNombreAerolinea() {
        return nombreAerolinea;
    }

    public void setNombreAerolinea(String nombreAerolinea) {
        this.nombreAerolinea = nombreAerolinea;
    }

    public String getVuelo() {
        return vuelo;
    }

    public void setVuelo(String vuelo) {
        this.vuelo = vuelo;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
}
