/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jonathan.rodriguez
 */
@Entity
@Table(name = "boleto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Boleto.findAll", query = "SELECT b FROM Boleto b")
    , @NamedQuery(name = "Boleto.findByIdTiquete", query = "SELECT b FROM Boleto b WHERE b.idTiquete = :idTiquete")
    , @NamedQuery(name = "Boleto.findByOrigen", query = "SELECT b FROM Boleto b WHERE b.origen = :origen")
    , @NamedQuery(name = "Boleto.findByDestino", query = "SELECT b FROM Boleto b WHERE b.destino = :destino")
    , @NamedQuery(name = "Boleto.findByNumeroPuesto", query = "SELECT b FROM Boleto b WHERE b.numeroPuesto = :numeroPuesto")
    , @NamedQuery(name = "Boleto.findByFechaVuelo", query = "SELECT b FROM Boleto b WHERE b.fechaVuelo = :fechaVuelo")
    , @NamedQuery(name = "Boleto.findByHoraSalida", query = "SELECT b FROM Boleto b WHERE b.horaSalida = :horaSalida")
    , @NamedQuery(name = "Boleto.findByCodigo", query = "SELECT b FROM Boleto b WHERE b.codigo = :codigo")
    , @NamedQuery(name = "Boleto.findByNombreUno", query = "SELECT b FROM Boleto b WHERE b.nombreUno = :nombreUno")
    , @NamedQuery(name = "Boleto.findByNombreDos", query = "SELECT b FROM Boleto b WHERE b.nombreDos = :nombreDos")
    , @NamedQuery(name = "Boleto.findByApellidoUno", query = "SELECT b FROM Boleto b WHERE b.apellidoUno = :apellidoUno")
    , @NamedQuery(name = "Boleto.findByApellidoDos", query = "SELECT b FROM Boleto b WHERE b.apellidoDos = :apellidoDos")})
public class Boleto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "id_tiquete")
    @Id
    private int idTiquete;
    @Column(name = "origen")
    private String origen;
    @Basic(optional = false)
    @Column(name = "destino")
    private String destino;
    @Column(name = "numero_puesto")
    private Integer numeroPuesto;
    @Column(name = "fecha_vuelo")
    private String fechaVuelo;
    @Column(name = "hora_salida")
    private String horaSalida;
    @Column(name = "codigo")
    private Integer codigo;
    @Column(name = "nombre_uno")
    private String nombreUno;
    @Column(name = "nombre_dos")
    private String nombreDos;
    @Column(name = "apellido_uno")
    private String apellidoUno;
    @Column(name = "apellido_dos")
    private String apellidoDos;

    public Boleto() {
    }

    public int getIdTiquete() {
        return idTiquete;
    }

    public void setIdTiquete(int idTiquete) {
        this.idTiquete = idTiquete;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Integer getNumeroPuesto() {
        return numeroPuesto;
    }

    public void setNumeroPuesto(Integer numeroPuesto) {
        this.numeroPuesto = numeroPuesto;
    }

    public String getFechaVuelo() {
        return fechaVuelo;
    }

    public void setFechaVuelo(String fechaVuelo) {
        this.fechaVuelo = fechaVuelo;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombreUno() {
        return nombreUno;
    }

    public void setNombreUno(String nombreUno) {
        this.nombreUno = nombreUno;
    }

    public String getNombreDos() {
        return nombreDos;
    }

    public void setNombreDos(String nombreDos) {
        this.nombreDos = nombreDos;
    }

    public String getApellidoUno() {
        return apellidoUno;
    }

    public void setApellidoUno(String apellidoUno) {
        this.apellidoUno = apellidoUno;
    }

    public String getApellidoDos() {
        return apellidoDos;
    }

    public void setApellidoDos(String apellidoDos) {
        this.apellidoDos = apellidoDos;
    }
    
}
