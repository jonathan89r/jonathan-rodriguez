/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimiento;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.LoginUsuario;

/**
 *
 * @author jonathan.rodriguez
 */
public class validacion_login {

    public LoginUsuario consultarLogin(String us, String pass) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query q = em.createNamedQuery("LoginUsuario.findByUsuarioPass")
                .setParameter("usuario", us)
                .setParameter("pass", pass);

        LoginUsuario log = null;

        List<LoginUsuario> lista = q.getResultList();

        for (LoginUsuario login : lista) {
            log = login;
        }
        em.close();
        return log;
    }

    public LoginUsuario verificar(String us, String respuesta, String pregunta) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query query = em.createNamedQuery("LoginUsuario.findByRecuperar", LoginUsuario.class)
                .setParameter("usuario", us)
                .setParameter("pregunta", pregunta)
                .setParameter("respuesta", respuesta);

        List<LoginUsuario> lista = query.getResultList();
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        em.close();

        return null;

    }

    public int actualizarPass(String pass, int id_login) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        int flag = 0;
        try {
            LoginUsuario lo = em.find(LoginUsuario.class, id_login);
            lo.setPass(pass);
            em.getTransaction().commit();
            flag = 1;
        } catch (Exception e) {
            em.getTransaction().rollback();
            flag = 2;
        } finally {
            em.close();
        }
        return flag;

    }
}
