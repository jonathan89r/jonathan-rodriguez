/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimiento;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.AvionDestino;
import persistencia.Destino;
import persistencia.Ofertasvuelos;
import persistencia.Origen;
import persistencia.OrigenDestino;
import persistencia.Vuelosv;

/**
 *
 * @author jonathan.rodriguez
 */
public class mantenimiento_filtro {

    public List consultarOrigen() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Origen> lista = null;

        try {
            Query q = em.createNamedQuery("Origen.findAll");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public List consultarDestino() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Destino> lista = null;

        try {
            Query q = em.createNamedQuery("Destino.findAll");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public List consultarOrigenDestino() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<OrigenDestino> lista = null;

        try {
            Query q = em.createQuery("SELECT o FROM OrigenDestino o");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public List consultarAvionDestino() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<AvionDestino> lista = null;

        try {
            Query q = em.createQuery("SELECT a FROM AvionDestino a");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public AvionDestino consultarVuelos(int id_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        AvionDestino objeto = null;
        try {
            objeto = em.find(AvionDestino.class, id_destino);

            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public AvionDestino consultarVuelos2(int id_origen) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        AvionDestino objeto = null;
        try {
            objeto = em.find(AvionDestino.class, id_origen);

            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public OrigenDestino consultarOD(int id_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        OrigenDestino objeto = null;
        try {
            objeto = em.find(OrigenDestino.class, id_destino);

            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public List consultarOfertas() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Ofertasvuelos> lista = null;

        try {
            Query q = em.createQuery("SELECT o FROM Ofertasvuelos o");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public Ofertasvuelos buscarOfertas(int id_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        Ofertasvuelos objeto = null;
        try {
            objeto = em.find(Ofertasvuelos.class, id_destino);

            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public List consultarVuelos() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Vuelosv> lista = null;

        try {
            Query q = em.createQuery("SELECT v FROM Vuelosv v");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public OrigenDestino buscarOD(int id_Destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query q = em.createNamedQuery("OrigenDestino.findOD2")
                .setParameter("idDestino", id_Destino);

        OrigenDestino lista = null;
        List<OrigenDestino> o = q.getResultList();

        for (OrigenDestino origenDestino : o) {
            lista = origenDestino;
        }

        em.close();

        return lista;
    }

    public AvionDestino buscarAD(int id_origen_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query q = em.createNamedQuery("AvionDestino.findOD")
                .setParameter("idOrigenDestino", id_origen_destino);

        AvionDestino lista = null;
        List<AvionDestino> o = q.getResultList();

        for (AvionDestino avionDestino : o) {
            lista = avionDestino;
        }

        em.close();

        return lista;
    }

    public Vuelosv buscarVuelosSV(int id_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        Vuelosv objeto = null;
        try {
            objeto = em.find(Vuelosv.class, id_destino);

            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public Vuelosv buscarOD2(int id_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query q = em.createNamedQuery("Vuelosv.findByDestino")
                .setParameter("idDestino", id_destino);

        Vuelosv lista = null;
        List<Vuelosv> o = q.getResultList();

        for (Vuelosv avionDestino : o) {
            lista = avionDestino;
        }

        em.close();

        return lista;
    }
}
