/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jonathan.rodriguez
 */
@Entity
@Table(name = "login_usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LoginUsuario.findAll", query = "SELECT l FROM LoginUsuario l")
    , @NamedQuery(name = "LoginUsuario.findByIdLogin", query = "SELECT l FROM LoginUsuario l WHERE l.idLogin = :idLogin")
    , @NamedQuery(name = "LoginUsuario.findByUsuario", query = "SELECT l FROM LoginUsuario l WHERE l.usuario = :usuario")
    , @NamedQuery(name = "LoginUsuario.findByPass", query = "SELECT l FROM LoginUsuario l WHERE l.pass = :pass")
    , @NamedQuery(name = "LoginUsuario.findByUsuarioPass", query = "SELECT l FROM LoginUsuario l WHERE l.usuario = :usuario AND l.pass = :pass")
    , @NamedQuery(name = "LoginUsuario.findByRecuperar", query = "SELECT l FROM LoginUsuario l WHERE l.usuario = :usuario AND l.pregunta = :pregunta AND l.respuesta = :respuesta")
    , @NamedQuery(name = "LoginUsuario.findByPregunta", query = "SELECT l FROM LoginUsuario l WHERE l.pregunta = :pregunta")
    , @NamedQuery(name = "LoginUsuario.findByRespuesta", query = "SELECT l FROM LoginUsuario l WHERE l.respuesta = :respuesta")})
public class LoginUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_login")
    private Integer idLogin;
    @Basic(optional = false)
    @Column(name = "usuario")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "pass")
    private String pass;
    @Column(name = "pregunta")
    private String pregunta;
    @Column(name = "respuesta")
    private String respuesta;
    @JoinColumn(name = "tipo_usuario", referencedColumnName = "id_tipo")
    @ManyToOne(optional = false)
    private TipoUsuario tipoUsuario;
    @OneToMany(mappedBy = "idUsuario")
    private Collection<Pasajero> pasajeroCollection;

    public LoginUsuario() {
    }

    public LoginUsuario(Integer idLogin) {
        this.idLogin = idLogin;
    }

    public LoginUsuario(Integer idLogin, String usuario, String pass) {
        this.idLogin = idLogin;
        this.usuario = usuario;
        this.pass = pass;
    }

    public Integer getIdLogin() {
        return idLogin;
    }

    public void setIdLogin(Integer idLogin) {
        this.idLogin = idLogin;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @XmlTransient
    public Collection<Pasajero> getPasajeroCollection() {
        return pasajeroCollection;
    }

    public void setPasajeroCollection(Collection<Pasajero> pasajeroCollection) {
        this.pasajeroCollection = pasajeroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLogin != null ? idLogin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoginUsuario)) {
            return false;
        }
        LoginUsuario other = (LoginUsuario) object;
        if ((this.idLogin == null && other.idLogin != null) || (this.idLogin != null && !this.idLogin.equals(other.idLogin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.LoginUsuario[ idLogin=" + idLogin + " ]";
    }

}
