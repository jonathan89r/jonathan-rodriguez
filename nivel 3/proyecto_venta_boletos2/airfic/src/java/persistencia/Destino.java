/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jonathan.rodriguez
 */
@Entity
@Table(name = "destino")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Destino.findAll", query = "SELECT d FROM Destino d")
    , @NamedQuery(name = "Destino.findByIdDestino", query = "SELECT d FROM Destino d WHERE d.idDestino = :idDestino")
    , @NamedQuery(name = "Destino.findByNombre", query = "SELECT d FROM Destino d WHERE d.nombre = :nombre")
    , @NamedQuery(name = "Destino.findByPhoto", query = "SELECT d FROM Destino d WHERE d.photo = :photo")})
public class Destino implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_destino")
    private Integer idDestino;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "photo")
    private String photo;
    @OneToMany(mappedBy = "idDestino")
    private Collection<OrigenDestino> origenDestinoCollection;

    public Destino() {
    }

    public Destino(Integer idDestino) {
        this.idDestino = idDestino;
    }

    public Destino(Integer idDestino, String nombre) {
        this.idDestino = idDestino;
        this.nombre = nombre;
    }

    public Integer getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(Integer idDestino) {
        this.idDestino = idDestino;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @XmlTransient
    public Collection<OrigenDestino> getOrigenDestinoCollection() {
        return origenDestinoCollection;
    }

    public void setOrigenDestinoCollection(Collection<OrigenDestino> origenDestinoCollection) {
        this.origenDestinoCollection = origenDestinoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDestino != null ? idDestino.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Destino)) {
            return false;
        }
        Destino other = (Destino) object;
        if ((this.idDestino == null && other.idDestino != null) || (this.idDestino != null && !this.idDestino.equals(other.idDestino))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.Destino[ idDestino=" + idDestino + " ]";
    }
    
}
