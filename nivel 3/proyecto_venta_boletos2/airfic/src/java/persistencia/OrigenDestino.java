/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jonathan.rodriguez
 */
@Entity
@Table(name = "origen_destino")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrigenDestino.findAll", query = "SELECT o FROM OrigenDestino o")
    , @NamedQuery(name = "OrigenDestino.findByIdOrigenDestino", query = "SELECT o FROM OrigenDestino o WHERE o.idOrigenDestino = :idOrigenDestino")
    , @NamedQuery(name = "OrigenDestino.findFiltro", query = "SELECT o FROM OrigenDestino o WHERE o.idOrigen = :idOrigen AND o.idDestino = :idDestino AND o.clase = :clase")
    , @NamedQuery(name = "OrigenDestino.findByPrecio", query = "SELECT o FROM OrigenDestino o WHERE o.precio = :precio")})
public class OrigenDestino implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_origen_destino")
    private Integer idOrigenDestino;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio")
    private Double precio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idOrigenDestino")
    private Collection<AvionDestino> avionDestinoCollection;
    @JoinColumn(name = "clase", referencedColumnName = "id_clase")
    @ManyToOne
    private Clase clase;
    @JoinColumn(name = "id_destino", referencedColumnName = "id_destino")
    @ManyToOne
    private Destino idDestino;
    @JoinColumn(name = "id_origen", referencedColumnName = "id_origen")
    @ManyToOne
    private Origen idOrigen;

    public OrigenDestino() {
    }

    public OrigenDestino(Integer idOrigenDestino) {
        this.idOrigenDestino = idOrigenDestino;
    }

    public Integer getIdOrigenDestino() {
        return idOrigenDestino;
    }

    public void setIdOrigenDestino(Integer idOrigenDestino) {
        this.idOrigenDestino = idOrigenDestino;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @XmlTransient
    public Collection<AvionDestino> getAvionDestinoCollection() {
        return avionDestinoCollection;
    }

    public void setAvionDestinoCollection(Collection<AvionDestino> avionDestinoCollection) {
        this.avionDestinoCollection = avionDestinoCollection;
    }

    public Clase getClase() {
        return clase;
    }

    public void setClase(Clase clase) {
        this.clase = clase;
    }

    public Destino getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(Destino idDestino) {
        this.idDestino = idDestino;
    }

    public Origen getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(Origen idOrigen) {
        this.idOrigen = idOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrigenDestino != null ? idOrigenDestino.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrigenDestino)) {
            return false;
        }
        OrigenDestino other = (OrigenDestino) object;
        if ((this.idOrigenDestino == null && other.idOrigenDestino != null) || (this.idOrigenDestino != null && !this.idOrigenDestino.equals(other.idOrigenDestino))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.OrigenDestino[ idOrigenDestino=" + idOrigenDestino + " ]";
    }
    
}
