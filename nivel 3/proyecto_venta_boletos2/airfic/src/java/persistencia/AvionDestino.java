/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jonathan.rodriguez
 */
@Entity
@Table(name = "avion_destino")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AvionDestino.findAll", query = "SELECT a FROM AvionDestino a")
    , @NamedQuery(name = "AvionDestino.findByIdAvionDestino", query = "SELECT a FROM AvionDestino a WHERE a.idAvionDestino = :idAvionDestino")
    , @NamedQuery(name = "AvionDestino.findfiltro", query = "SELECT a FROM AvionDestino a WHERE a.idOrigenDestino = :idOrigenDestino")
})
public class AvionDestino implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_avion_destino")
    private Integer idAvionDestino;
    @JoinColumn(name = "id_origen_destino", referencedColumnName = "id_origen_destino")
    @ManyToOne(optional = false)
    private OrigenDestino idOrigenDestino;
    @JoinColumn(name = "tipo_vuelo", referencedColumnName = "id_tipo_vuelo")
    @ManyToOne(optional = false)
    private TipoVuelo tipoVuelo;
    @JoinColumn(name = "id_avion", referencedColumnName = "id_avion")
    @ManyToOne(optional = false)
    private Avion idAvion;
    @OneToMany(mappedBy = "desdeHacia")
    private Collection<Tiquete> tiqueteCollection;

    public AvionDestino() {
    }

    public AvionDestino(Integer idAvionDestino) {
        this.idAvionDestino = idAvionDestino;
    }

    public Integer getIdAvionDestino() {
        return idAvionDestino;
    }

    public void setIdAvionDestino(Integer idAvionDestino) {
        this.idAvionDestino = idAvionDestino;
    }

    public OrigenDestino getIdOrigenDestino() {
        return idOrigenDestino;
    }

    public void setIdOrigenDestino(OrigenDestino idOrigenDestino) {
        this.idOrigenDestino = idOrigenDestino;
    }

    public TipoVuelo getTipoVuelo() {
        return tipoVuelo;
    }

    public void setTipoVuelo(TipoVuelo tipoVuelo) {
        this.tipoVuelo = tipoVuelo;
    }

    public Avion getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(Avion idAvion) {
        this.idAvion = idAvion;
    }

    @XmlTransient
    public Collection<Tiquete> getTiqueteCollection() {
        return tiqueteCollection;
    }

    public void setTiqueteCollection(Collection<Tiquete> tiqueteCollection) {
        this.tiqueteCollection = tiqueteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAvionDestino != null ? idAvionDestino.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AvionDestino)) {
            return false;
        }
        AvionDestino other = (AvionDestino) object;
        if ((this.idAvionDestino == null && other.idAvionDestino != null) || (this.idAvionDestino != null && !this.idAvionDestino.equals(other.idAvionDestino))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "persistencia.AvionDestino[ idAvionDestino=" + idAvionDestino + " ]";
    }
    
}
