/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBean;

import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import mantenimiento.validacion_login;
import persistencia.LoginUsuario;

/**
 *
 * @author jonathan.rodriguez
 */
@ManagedBean
@RequestScoped
@SessionScoped
public class bean_recuperacion {

    private LoginUsuario l;
    private LoginUsuario log;
    private validacion_login v = new validacion_login();
    private String pass, pss2;

    @PostConstruct
    public void init() {
        this.l = new LoginUsuario();
        log = (LoginUsuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("aa");

    }

    public String recuperar() throws IOException {
        l = v.verificar(l.getUsuario(), l.getRespuesta(), l.getPregunta());
        String mensaje = "";
        if (l != null) {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("aa", l);
            if (l.getRespuesta() != null && l.getPregunta() != null && l.getUsuario() != null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("nueva_contraseña.xhtml?faces-redirect-true");

            } else {
                mensaje = "Error, la respuesta es erronea";
            }
        } else {
            mensaje = "Error, no se encontro el usuario";
        }
        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return null;
    }

    public void cambiarContra(int id) throws IOException {
        String mensaje = "";

        if (this.pass.equals(this.pss2)) {
            if (v.actualizarPass(this.pass, log.getIdLogin()) == 1) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml?faces-redirect-true");
            } else {
                mensaje = "Error al actualizar";
            }
        } else {
            mensaje = "Contraseñas no coinciden";
        }

        FacesMessage msg = new FacesMessage(mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    /**
     * @return the l
     */
    public LoginUsuario getL() {
        return l;
    }

    /**
     * @param l the l to set
     */
    public void setL(LoginUsuario l) {
        this.l = l;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return the pss2
     */
    public String getPss2() {
        return pss2;
    }

    /**
     * @param pss2 the pss2 to set
     */
    public void setPss2(String pss2) {
        this.pss2 = pss2;
    }

    /**
     * @return the log
     */
    public LoginUsuario getLog() {
        return log;
    }

    /**
     * @param log the log to set
     */
    public void setLog(LoginUsuario log) {
        this.log = log;
    }

}
