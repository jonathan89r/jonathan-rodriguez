/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimiento;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import persistencia.AvionDestino;
import persistencia.Clase;
import persistencia.Destino;
import persistencia.Ofertasvuelos;
import persistencia.Origen;
import persistencia.OrigenDestino;
import persistencia.Vuelosv;

/**
 *
 * @author jonathan.rodriguez
 */
public class mantenimiento_filtro {

    public List mostrarOrigen() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Origen> lista = null;

        try {
            Query q = em.createNamedQuery("Origen.findAll");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public List mostrarDestino() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Destino> lista = null;

        try {
            Query q = em.createNamedQuery("Destino.findAll");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public List mostrarOrigenDestino() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<OrigenDestino> lista = null;

        try {
            Query q = em.createQuery("SELECT o FROM OrigenDestino o");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public List consultarAvionDestino() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<AvionDestino> lista = null;

        try {
            Query q = em.createQuery("SELECT a FROM AvionDestino a");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public List consultarOfertas() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Ofertasvuelos> lista = null;

        try {
            Query q = em.createQuery("SELECT o FROM Ofertasvuelos o");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public Ofertasvuelos buscarOfertas(int id_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        Ofertasvuelos objeto = null;
        try {
            objeto = em.find(Ofertasvuelos.class, id_destino);

            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public List consultarVuelos() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Vuelosv> lista = null;

        try {
            Query q = em.createQuery("SELECT v FROM Vuelosv v");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public Vuelosv buscarVuelosSV(int id_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        Vuelosv objeto = null;
        try {
            objeto = em.find(Vuelosv.class, id_destino);

            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public List mostrarClases() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Clase> lista = null;
        try {

            Query q = em.createQuery("SELECT c FROM Clase c");

            lista = q.getResultList();
            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public OrigenDestino consultarOrigenDestino(Destino destino, Origen origen, Clase clase) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query q = em.createNamedQuery("OrigenDestino.findFiltro")
                .setParameter("idOrigen", origen)
                .setParameter("idDestino", destino)
                .setParameter("clase", clase);

        OrigenDestino log = null;

        List<OrigenDestino> lista = q.getResultList();

        for (OrigenDestino login : lista) {
            log = login;
        }
        em.close();
        return log;
    }

    public AvionDestino consultarAvionDestino(OrigenDestino origen_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query q = em.createNamedQuery("AvionDestino.findfiltro")
                .setParameter("idOrigenDestino", origen_destino);

        AvionDestino log = null;

        List<AvionDestino> lista = q.getResultList();

        for (AvionDestino login : lista) {
            log = login;
        }
        em.close();
        return log;
    }

}
