/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mantenimiento;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import persistencia.AvionDestino;
import persistencia.Pasajero;
import persistencia.TipoDocumento;
import persistencia.Tiquete;
import persistencia.Vuelosv;

/**
 *
 * @author jonathan.rodriguez
 */
public class mantenimiento_tiquete {

    public int guardarTiquete(
            int id_usuario,
            String nombre_uno,
            String nombre_dos,
            String apellido_uno,
            String apellido_dos,
            String telefono,
            String direccion,
            int id_documento,
            String documento,
            Double valor,
            String fecha_vuelo,
            Double iva,
            Double total,
            String hora_salida,
            int destino,
            int codigo,
            int tipo_pago
    ) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        int flag = 0;

        StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("boleto");

        storedProcedure.registerStoredProcedureParameter("xid_usuario", Integer.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xN_1", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xN_2", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xA_1", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xA_2", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xTelefono", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xDireccion", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xT_documento", Integer.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xDocumento", String.class, ParameterMode.IN);

        storedProcedure.registerStoredProcedureParameter("xvalor", Double.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xfecha_vuelo", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("Xiva", Double.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xTotal", Double.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xHora_salida", String.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xdestino", Integer.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xcodigo", Integer.class, ParameterMode.IN);
        storedProcedure.registerStoredProcedureParameter("xTipo_pago", Integer.class, ParameterMode.IN);

        storedProcedure.setParameter("xid_usuario", id_usuario);
        storedProcedure.setParameter("xN_1", nombre_uno);
        storedProcedure.setParameter("xN_2", nombre_dos);
        storedProcedure.setParameter("xA_1", apellido_uno);
        storedProcedure.setParameter("xA_2", apellido_dos);
        storedProcedure.setParameter("xTelefono", telefono);
        storedProcedure.setParameter("xDireccion", direccion);
        storedProcedure.setParameter("xT_documento", id_documento);
        storedProcedure.setParameter("xDocumento", documento);

        storedProcedure.setParameter("xvalor", valor);
        storedProcedure.setParameter("xfecha_vuelo", fecha_vuelo);
        storedProcedure.setParameter("Xiva", iva);
        storedProcedure.setParameter("xTotal", total);
        storedProcedure.setParameter("xHora_salida", hora_salida);
        storedProcedure.setParameter("xdestino", destino);
        storedProcedure.setParameter("xcodigo", codigo);
        storedProcedure.setParameter("xTipo_pago", tipo_pago);

        boolean r = storedProcedure.execute();
        if (r) {
            flag = 1;
        } else {
            flag = 0;
        }

        return flag;
    }

    public List consultarTDocumento() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<TipoDocumento> lista = null;

        try {
            Query q = em.createQuery("SELECT t FROM TipoDocumento t");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public AvionDestino consultarAD(int id) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        AvionDestino objeto = null;
        try {
            objeto = em.find(AvionDestino.class, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

//    public int guardarTPago(String pago) {
//        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
//        int flag;
//        
//        StoredProcedureQuery storedProcedure = em.createStoredProcedureQuery("prueba");
//        storedProcedure.registerStoredProcedureParameter("xPago", String.class, ParameterMode.IN);
//        storedProcedure.setParameter("xPago", pago);
//        boolean r = storedProcedure.execute();
//        if (r == false) {
//            flag = 1;
//        } else {
//            flag = 0;
//        }
//        return flag;
//    }
    public Tiquete consultarTiquete(int id) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();

        Tiquete objeto = null;
        try {
            objeto = em.find(Tiquete.class, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public List consultarAvionDestino() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<AvionDestino> lista = null;

        try {
            Query q = em.createQuery("SELECT a FROM AvionDestino a");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public List consultarVuelossv() {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        List<Vuelosv> lista = null;

        try {
            Query q = em.createQuery("SELECT v FROM Vuelosv v");
            lista = q.getResultList();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return lista;
    }

    public Vuelosv buscarVuelosSV(int id_destino) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        Vuelosv objeto = null;
        try {
            objeto = em.find(Vuelosv.class, id_destino);

            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();

        } finally {
            em.close();
        }
        return objeto;
    }

    public Tiquete verificarBoleto(String us, String respuesta, String pregunta) {
        EntityManager em = jpautil.getEntityManagerFactory().createEntityManager();

        Query query = em.createNamedQuery("Tiquete.findAll", Tiquete.class)
                .setParameter("usuario", us)
                .setParameter("pregunta", pregunta)
                .setParameter("respuesta", respuesta);

        List<Tiquete> lista = query.getResultList();
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        em.close();

        return null;

    }
}
