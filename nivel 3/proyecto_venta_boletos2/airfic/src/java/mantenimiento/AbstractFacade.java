///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package mantenimiento;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityTransaction;
//
///**
// *
// * @author jonathan.rodriguez
// */
//public abstract class AbstractFacade <T>{
//    
//    private Class<T> entityClass;
//
//    public AbstractFacade(Class<T> entityClass) {
//        this.entityClass = entityClass;
//    }
//    // declaracion de un metodo astracto
//    protected abstract EntityManager getEtEnttityManager();
//    
//    public void create(T entity){
//        EntityManager em=getEtEnttityManager();
//        EntityTransaction et=null;
//        try {
//            et=em.getTransaction();
//            et.begin();
//            em.persist(entity);
//            et.commit();
//            
//        } catch (Exception e) {
//            if (et!=null && et.isActive()) {
//                et.rollback();
//            }
//        }finally{
//            if (em!=null && em.isOpen()) {
//                em.close();
//            }
//        }
//    }
//    
//    
//    
//}
